# AV Course Log

## Build and Pre-code Instructions

### Car build
1. Build car

2. Flash Tegra

3. Configure Motor Control
   - Load XML [`traxxas_velineon_3500.xml`](https://github.com/mit-racecar/hardware/blob/master/vesc/traxxas_velineon_3500.xml) using the [BLDC tool](https://github.com/vedderb/bldc-tool).
   - Flash a the updated firmware if the steering does not work. The .bin files are in the same repository.

4. Networking with Tegra

5. Testing movement with Teleop
    - Raise the car off the group with blocks for first tests.
    - Run: `roslaunch racecar teleop.launch` on the Tegra.
        - should the vesc not be plugged in, an error will occur. Plug it in, then restart the teleop.
    - Using the wireless (USB attached) controller control the car using the analog sticks.
      - Left-upper trigger is a dead man switch for operation.
      - Left analog controls speed, right analog controls steering.
      - The switch on the back of the controller should be on 'X'.
      - If movement does not work, check press "Mode" to switch the left of the controller from digital to analog input and check the correct XML files are loaded onto the motor controller.


### Shutting down the car.

- Run a normal ubuntu shutdown: `sudo shutdown now -h`
- Unplug both power-supplying cables from the computer battery.
- Unplug the motor controller from the car battery

### Gazebo

#### Setup and first run

This assumes you have already [installed ROS](http://wiki.ros.org/ROS/Installation).

Terminal commands:
```bash
cd ~
git clone https://github.com/mit-racecar/racecar
mv racecar/ racecar-ws

cd racecar-ws/
mv racecar-vm.rosinstall .rosinstall # on laptop or VM, not Tegra

rosinstall .

sudo apt-get install ros-kinetic-ackermann-msgs
sudo apt-get install ros-kinetic-serial

catkin_make

# assuming successful...

echo "source ~/racecar-ws/devel/setup.bash" >> ~/.bashrc
```

That will get all the racecar stuff needed to get it into Gazebo.

To run: `$ roslaunch racecar_gazebo racecar_tunnel.launch`

#### OLD DEPRECATED! Fixing Possible MIT VM Image Gazebo Errors
(by: Jonathan Morris)

When using the default MIT image, several errors occur when trying to use Gazebo and the racecar_gazebo launch files. These steps aim to rectify those problems. This guide will work assuming you are using a fresh copy of the `RSS-VM-32BITS-disk1.vmdk` image. Ensure the VM is connected to the ‘DHCP’ network for this procedure.
We will update the installed packages (1,2), update the ROS installation from GitHub (3-6), install missing gazebo controller plugins (7), clone the racecar-simulator directory from GitHub (8), run catkin_make to build the racecar workspace (9), and finally edit the `~/.bashrc` file to source setup.bash automatically.

1. `sudo apt-get update`
2. `sudo apt-get upgrade`
3. `cd ~/racecar-ws`
4. `rm -f .rosinstall``
5. `wget https://raw.githubusercontent.com/mit-racecar/racecar/master/racecar.rosinstall -O .rosinstall`
6. `rosws update``
7. `sudo apt-get install ros-indigo-youbot-gazebo-robot ros-indigo-youbot-gazebo-control ros-indigo-youbot-description ros-indigo-youbot-driver ros-indigo-youbot-driver-ros-interface ros- indigo-youbot-gazebo-worlds ros-indigo-youbot-simulation ros-indigo-gazebo-ros-control ros- indigo-effort-controllers ros-indigo-joint-state-controller ros-indigo-joint-trajectory-controller`
8. `git clone https://github.com/mit-racecar/racecar-simulator.git ~/racecar-ws/src/racecar- simulator`
9. `catkin_make`
10. `echo "source ~/racecar-ws/devel/setup.bash" >> ~/.bashrc`

Now, test your setup. Change the connected network to 'RACECAR-Static'. Run: `roslaunch racecar_gazebo racecar_tunnel.launch`

If gazebo complaining about missing models annoys/hinders you, then do the following:

1. `mkdir ~/.gazebo/models`
2. Copy the contents of the repository from https://bitbucket.org/osrf/gazebo_models/downloads to the newly created directory


## Programming examples and discussion

### Basic Ackermann motor and steering control

A basic example publishing an [`AckermannDriveStamped`](http://docs.ros.org/kinetic/api/ackermann_msgs/html/msg/AckermannDriveStamped.html) message to the input navigation topic. Update the [`AckermannDrive`](http://docs.ros.org/kinetic/api/ackermann_msgs/html/msg/AckermannDrive.html) variable `drive`.

```python
# Copyleft 2016 Keely Hill

import rospy

from ackermann_msgs.msg import AckermannDriveStamped

class GoForwardTurning():
    def __init__(self):

        rospy.init_node('GoForwardTurning', anonymous=False)

        rospy.loginfo("To stop: CTRL + C")
        # What function to call when you ctrl + c
        rospy.on_shutdown(self.shutdown)

        # `navigation` is a lower priority than `teleop`, this allows emergency overriding
        self.cmd_vel = rospy.Publisher(
                            '/vesc/ackermann_cmd_mux/input/navigation',
                            AckermannDriveStamped, queue_size=10)

        r = rospy.Rate(10)  # handles 10 times per second

        # change this variable (`move_cmd.drive`) to move and steer
        move_cmd = AckermannDriveStamped()
        move_cmd.drive.speed = 1.5
        move_cmd.drive.steering_angle = 0.5 # radians

        # as long as you haven't ctrl + c keeping doing...
        while not rospy.is_shutdown():

            self.cmd_vel.publish(move_cmd)

            r.sleep()

    def shutdown(self):

        rospy.loginfo("Stoping...")

        self.cmd_vel.publish(AckermannDriveStamped()) # zero all commands

        # sleep just makes sure it receives the stop command prior to shutting down
        rospy.sleep(1)

if __name__ == '__main__':
    try:
        GoForwardTurning()
    except Exception as e:
        print("Exception: "+ str(e))
        rospy.loginfo("GoForwardTurning node terminated.")

```

To run, first: `roslaunch racecar teleop.launch`; this starts the core and initiates sensors and topics.
Then simply run the python script as another process.

#### Adding subscribers to sensors

Every topic subscription calls a given callback in realtime with the message as the argument. The bellow can be added to the GoForwardTurning class example, or work in any class. Determine the message type with `rostopic info /the/topic/path` or `rqt_graph` or any means. Search the ROS docs for the message type to learn how it gives data.

```python
from sensor_msgs.msg import LaserScan # for use in subscribing

# ... somewhere in __init__:

# pass: topic, type (class) of message from import, and any callback.
rospy.Subscriber('/scan', LaserScan, self.scan_CB)

# ... somewhere in class body:

def scan_CB(self, scan):
    # get middle range, for example
    scan.ranges[len(scan.ranges)/2]

```


### UInt8 image data to CV image

Getting ZED publishing: `roslaunch zed_wrapper`

```python
import cv_bridge

self.bridge = cv_bridge.CvBridge()

def zed_depth_image_CB(self, imgmsg):
    image = self.bridge.imgmsg_to_cv2(imgmsg, desired_encoding='bgr8')

```

```python
# !!! this does not work with kinetic !!!

import cv2
import numpy as np

# callback of subscriber
def zed_compressed_CB(self, img_msgs):
    img = cv2.imdecode(np.fromstring(img_msgs.data, np.uint8), cv2.CV_LOAD_IMAGE_COLOR)
    # that's it
```


### Wall centering

One approach to staying centered between to walls is sampling the sides of the LIDAR range data. The data of each side is compared to find and error, which is used to steer to lessen the error.
My (Keely) first approach was average a third on each side. However, the large range of data points caused the car to sway in the center. By sampling a single point on each side, centering was smooth and consistent. The disadvantage to this the usage of less data. It works fine when the course is known to have consistent walls and no weird occurrences. Keep in mind: both of these method have no forward collision avoidance.
