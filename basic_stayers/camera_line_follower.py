from Rudolph import Rudolph
import cv2, cv_bridge
import numpy as np

class LineFollow(Rudolph):

    def __init__(self):
        Rudolph.__init__(self)
        self.acker.speed = 0

        self.bridge = cv_bridge.CvBridge()


    def zed_rgb_CB(self, imgmsg):

        img = self.bridge.imgmsg_to_cv2(imgmsg, desired_encoding='bgr8')

        #self.imgshow(img)

        self.steer_track_line(img)

    def zed_depth_image_CB(self, imgmsg):
    #img = self.bridge.imgmsg_to_cv2(imgmsg, desired_encoding='16C1')

        #self.imgshow(img)
    pass



    def steer_track_line(self, image):

        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    #hsv=image

        lower_yellow = np.array([26, 20,  26])
        upper_yellow = np.array([90,  120, 120])
        mask = cv2.inRange(hsv, lower_yellow, upper_yellow)

        # crop to only deal with bottom of mask
        h, w, d = image.shape
        search_top = 3*h/4
        search_bot = 3*h/4 + 20
       # mask[0:search_top, 0:w] = 0 # removes some top
       # mask[search_bot:h, 0:w] = 0 # removes some bottom

        M = cv2.moments(mask)
        if M['m00'] > 0:
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            cv2.circle(image, (cx, cy), 20, (0,0,255), -1)

            # find error, and steer
            err = cx - (w/2 + 75) # 75 is an offset for right camera

            self.acker.speed = 0
            self.acker.steering_angle =  -float(err) * 0.002


        self.imgshow(image)
    self.imgshow(mask, 'mask')


if __name__ == '__main__':
    try:
        lf = LineFollow()
        lf.start()
    except Exception as e:
        print("Exception: "+ str(e))
