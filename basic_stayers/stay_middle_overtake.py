"""stay_middle_overtake.py
Will attempt to overtake another car within range. also will do everything in the stay_middle class, and only
makes adjustments to it's steeering if the lidar values are in a 8 by -8 range.
"""

from Rudolph import Rudolph
import cv2
import numpy as np


class StayMiddle(Rudolph):
    def __init__(self):
        Rudolph.__init__(self)
        self.acker.speed = 0
        self.carDirection = 0

    def scan_CB(self, scan):

        scans_len = len(scan.ranges)
        right = scan.ranges[scans_len / 3]
        left = scan.ranges[2 * scans_len / 3]

        middle_view_min = np.average(scan.ranges[2 * scans_len / 5: 3 * scans_len / 5])

        err = round(right - left, 2) / 2
        isPassing = False

        # print(left, right, middle_view_min, err)


        # Overpass an opponent
if (8 <= self.acker.speed <= 30 and 3 < middle_view_min < 4) and (5 < left and 5 < right):
    self.acker.speed = 11
    print('I am attempting to pass an opponent. C:')
    isPassing = True

    while isPassing:

        if 3 < middle_view_min <= 4 and right - left > 0:
            self.acker.steering_angle = -0.3 * err
            self.acker.speed = 20
            print('Accelerating to the left!')

        elif 3 < middle_view_min <= 4 and right - left < 0:
            self.acker.steering_angle = 0.3 * err
            self.acker.speed = 20
            print('Accelerating to the right!')

        elif 1 < middle_view_min < 1.8:
            steer_direction = -1 if right - left > 0 else 1
            self.acker.steering_angle = 0.7 * steer_direction
            self.acker.speed = 10.0
            print('Hard Turn!!!')

            while (1 < left < 5 or 1 < right < 5) and middle_view_min > 4:

                if left + 0.05 > right + 0.05:
                    self.acker.steering_angle = 0.3 * err
                    print('Steering to the left!')

                elif left + 0.05 < right + 0.05:
                    self.acker.steering_angle = 0.3 * err
                    print('Steering to the right!')

                else:
                    self.acker.steering_angle = 0.0
                    self.acker.speed = 20
                    print('Going fast!')

            isPassing = False
            Print('Passing attempt failed. :C')


        elif 0.5 <= middle_view_min <= 1:
            self.acker.speed = 1.0
            isPassing = False
            Print('Passing attempt failed. :C')

        else:
            self.acker.speed = 12.0
            print('Passing attempt succesful! :D')


# turn away from wall
if 1 < middle_view_min < 1.8:
    steer_direction = -1 if right - left > 0 else 1
    self.acker.steering_angle = 0.6 * steer_direction
    self.acker.speed = 5.0
    print('there is a wall in front of me. ;~;')



# Do a three point turn
elif 0 <= middle_view_min <= 1:
    self.acker.speed = -5.0
    steer_direction = -1 if right - left > 0 else 1
    self.acker.steering_angle = -0.6 * steer_direction
    print('I am making a 3 point turn. :D')

    # Turn around until it is away from the wall
    while 1 < middle_view_min < 1.5:
        steer_direction = -1 if right - left > 0 else 1
        self.acker.steering_angle = -0.6 * steer_direction
        self.acker.speed = 1.0
        print('I am turning now. yay!')



# stay in middle of walls if the left and right averages are within range.
elif -8 <= right - left <= 8:
    self.acker.steering_angle = -0.2 * err
    self.acker.speed = 10
    print('I am staying in the middle :D')


# if the values are out of range, it will go straight. This is in case of glass, or very large spaces where the lidar cannot get
# an accurate reading of where to go.
else:
    self.acker.speed = 10
    print('HELP! I cannot see anything to the sides of me! :L')

if __name__ == '__main__':
    try:
        sm = StayMiddle()
        sm.start()
    except Exception as e:
        print("Exception: " + str(e))
