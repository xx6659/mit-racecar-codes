"""stay_middle.py
Uses two points on the forward sides of the LIDAR to get the cars position
in a hallway. It then steers to center itself.
"""

from Rudolph import Rudolph
import cv2
import numpy as np

class StayMiddle(Rudolph):

    def __init__(self):
        Rudolph.__init__(self)
        self.acker.speed = 0


    def scan_CB(self, scan):

        scans_len = len(scan.ranges)
        right = scan.ranges[scans_len/3]
        left = scan.ranges[2*scans_len/3]

        middle_view_min = np.average(scan.ranges[2*scans_len/5 : 3*scans_len/5])

        err = round(right-left, 2) / 2

        print(left, right, middle_view_min, err)

        if middle_view_min < 2: # turn away from wall
            steer_direction = -1 if right-left > 0 else 1
            self.acker.steering_angle = 0.6 * steer_direction
            self.acker.speed = 0.5
            print('there is a wall in front of me')

        else: # stay in middle of walls

            self.acker.steering_angle = -0.2 * err
            self.acker.speed = 10

            print('normal staying')


if __name__ == '__main__':
    try:
        sm = StayMiddle()
        sm.start()
    except Exception as e:
        print("Exception: "+ str(e))
