import subprocess

"""Small class for playing and handling sounds with a special names.
Very platform specific

Usage:
>>> sound = Sounder()
>>> sound.play('ready')
"""

class Sounder:

    name_to_file = {
        'ready': 'ready-for-action.wav',
        'auto-start': 'here-i-go.wav',
        'auto-stop': 'you-take-over.wav',
        'dead-ahead': 'something-dead-ahead.wav'
    }

    something_ahead_played = False

    def play(self, name):

        if name in self.name_to_file:

            if name == 'dead-ahead' and self.something_ahead_played:
                return
            elif name == 'dead-ahead':
                self.something_ahead_played = True

            filename = self.name_to_file[name]
            print('TODO do play: %s' % filename)
            subprocess.call(['afplay', '/Users/Keely/Desktop/Keely Voice/' + filename])
            # subprocess.call(['aplay', '~/kvoice/' + filename])
        else:
            print('No sound named: %s' % name)
