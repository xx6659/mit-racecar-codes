

#This is a python file that tests the steering algorithim I created to compensate for our robot
#turning at high speeds.
#This algorithim does not take account for friction or air resistance, unfortunately.
#A gyro would make that possible.

#TODO: Create a simulator for testing physics algorithims

class Physics:
    """Behold, this is a test module for the physics algorithim I made for the robot. THIS IS JUST A TEST TO
    MAKE SURE ALL OF THE VALUES CHECK. THIS HAS NOT BEEN IMPLEMENTED YET ON THE ROBOT."""
    def __init__(self, initvel=0.0, inittime=0.0, finaltime=1.0, initangle=0.0, left=0.0, right=0.0, middle=0.0):
        self.steer = initangle

        #This would be the starting velocity, or the acker.speed before calculations are made
        self.initVel = initvel

        #this is the velocity after the calculations are made, will be set as soon as the turn function is called
        self.finalVel = 0.0

        #The initial starting time, should be 0.
        self.initTime = inittime

        #The ending time, should be equal to the rate at which the robot publishes, converted to seconds
        self.finalTime = finaltime

        #initial angle is equal to the initial steering angle before the calculations are made
        self.initAngle = initangle

        #The final turning angle. this will be calculated when the turn function is called
        self.finalAngle = 0.0

        #This value will be set later on based on which side the robot is closest to
        self.radius = 0.0

        #These are the acceleration values, for testing purposes. will be used later on.
        self.accel = 0.0
        self.angularAcc = 0.0

        #The simulated acker.speed value that will be set to make the robot go forward.
        self.speed = 0.0

        #simulated distances on an x y coordinate plane
        #The simulator does not work yet, so this does not serve any purpose at the moment
        self.dist = 0.0
        self.xDist = 0.0
        self.yDist = 0.0

        # How far apart the walls are from each other
        # The simulator does not work yet, so this does not serve any purpose at the moment
        self.wallDist = 5.0

        # how long the track is, for testing
        # The simulator does not work yet, so this does not serve any purpose at the moment
        self.trackLen = 20.0

        # degree the turn is on, mostly for testing
        # The simulator does not work yet, so this does not serve any purpose at the moment
        self.turnDegree = 90

        #Simulated LIDAR ranges, for testing
        self.left = left
        self.right = right
        self.middle = middle

        #the direction it should steer itself
        self.steeringAngle = -1 if self.right - self.left > 0 else 1

        #The error
        self.err = round(right-left, 2) / 2


        #Test method for determining when and how the robot should turn
    def turn(self):

        #If the robot is close to a wall
        if  1 < self.middle < 2.0:

            # Checks what side the robot is closest to, and use that as the radius
            #If the left and right values are less then 0.001 (Basically zero)
            #Then set the speed value at -1.0 and the turn angle at 0.0
            if self.left <= 0.001 or self.right <= 0.001:
                print('I am too close to a wall. Going in reverse to decrease damage')
                self.speed = -0.5
                self.steer = -self.steer

                #Break out of the turning method, and possibly go do another action
                return

            # I added and subtracted -.1 to decrease the sensitivity of how it will choose between
            # The left and right values
            elif self.left + -.001 < self.right + .001:
                self.radius = self.left

            else:
                self.radius = self.right


            #The change in time calculated here.
            self.timeChange = (self.finalTime - self.initTime)

            #Sets the angular velocity equal to the initial velocity over the radius.
            self.angularVel = self.initVel / self.radius

            #Sets the final angle as the initial turning angle plus the initial angular velocity times the time
            #times pi, all over 180 times the steering angle.
            self.finalAngle = self.initAngle + (((self.angularVel * self.timeChange * self.steeringAngle) * 3.1459)/(180))
            print("The calculated angle in radians = " + str(self.finalAngle))

            #Makes sure that the robot does not oversteer, and if the bot does go over, it sets the value accordingly.
            if -0.5 < self.finalAngle < 0.5:
                self.steer = round(self.finalAngle, 2)
                print("The steer angle that was set in radians = " + str(self.steer))

            #If it isn't, it sets the steering equal to the maximum angle that it can steer at.
            else:
                self.steer = 0.5 * self.steeringAngle
                print("The final angle in radians = " + str(self.steer))


            #Sets the final velocity as the radius times the absolute value of the final angle minus the initial angle times 180 over
            #the change in time times pi.
            self.finalVel = self.radius * ((abs(self.steer) - self.initAngle) * 180 / ((self.timeChange + .1) * 3.1459))

            #Sets the acceleration
            self.angularAcc = self.finalVel-self.initVel
            self.accel = self.angularAcc

            print("The calculated speed in m/s = " + str(self.finalVel))


            #Makes sure the speed is not inbetween the values -.5 and .5
            if  self.finalVel < .5:
                # Set the value of the speed to .5
                self.speed = .5

            #If the speed value is high enough to where it will go
            else:
                self.speed = round(self.finalVel, 2)

            print("The speed value that was set = " + str(self.speed))

        # Backup
        elif 0 <= self.middle <= 1:
            self.speed = -5.0
            self.steer = -0.6 * self.steeringAngle
            print('I am backing up. :D')


        # stay in middle of walls if the left and right averages are within range.
        elif -8 <= self.right - self.left <= 8:
            self.steer = -0.2 * self.err
            self.speed = 10
            print('I am staying in the middle :D')


        # if the values are out of range, it will go straight. This is in case of glass, or very large spaces where the lidar cannot get
        # an accurate reading of where to go.
        else:
            self.speed = 10
            print('HELP! I cannot see anything to the sides of me! :L')


'''
if __name__ == '__main__':

    #set values to test what the robot will do on this delcared Physics object.
    physics = Physics(initangle=0.0, initvel=5.0, left=2.0, right=.0001, middle=1.9)

    i = 0
    physics.turn()

    # This loop is a timer to simulate where the robot will be after a second.
    # currently commented out, because the physics tester has not been finished yet
    #while i < 10:
        #physics.initTime = i
        #physics.finalTime = i + 1
        #physics.turn()

        #Find the distance traveled in the x direction(Does not simulate a track yet)
        #physics.dist = physics.finalVel + ((1/2) * (physics.accel * physics.finalTime)**2)
        #physics.xDist = physics.dist

'''


