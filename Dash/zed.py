import cv2
import numpy as np
import rospy
from sensor_msgs.msg import Image, PointCloud2


#The ZED Class
class ZED:
    def __init__(self):
        #Declare all of the local variables to use in here

        #Subscribes to the ZED Camera
        rospy.Subscriber('/zed/rgb/image_rect_color', Image, self.zed_rgb_CB)
        rospy.Subscriber('/zed/depth/depth_registered', Image, self.zed_depth_image_CB)
        rospy.Subscriber('/zed/point_cloud/cloud_registered', PointCloud2, self.zed_depth_cloud_CB)
   

    def zed_rgb_CB(self, data):
        #Insert what variables you want it to return here
        pass

    def zed_depth_image_CB(self, data):
        #Insert what variables you want it to return here
        pass

    def zed_depth_cloud_CB(self, data):
        #Insert what variables you want it to return here
        pass