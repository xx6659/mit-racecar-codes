import rospy
import cv2
from lidar import LIDAR
from zed import ZED
from driftingphysics import Physics
from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import Joy

class Commander(ZED, LIDAR):
    def __init__(self):

        #Initialize all of the sensors and start them
        ZED.__init__(self)
        LIDAR.__init__(self)
        self.physics = 0.0


        #Initialize the node
        rospy.init_node('Rudolph', anonymous=False)

        rospy.loginfo("To stop: ^C")
        rospy.on_shutdown(self.shutdown)

        # Ackermann drive pub, and zero it
        self.acker_pub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped, queue_size=10)

        self.ackermannMsg = AckermannDriveStamped()

        self.acker = self.ackermannMsg.drive
        self.acker.speed = 0
        self.acker.steering_angle = 0
        
        self.is_auto = False
        rospy.Subscriber('/vesc/joy', Joy, self.joy_CB)


    #The joystick method
    def joy_CB(self, joy):
        if joy.buttons[0] == 1: # A
            self.is_auto = True

        elif joy.buttons[2] == 1: # X
            self.acker.speed = 0
            self.publish_acker()
            self.is_auto = False

    #The ackermann publishing method
    def publish_acker(self):
        if self.is_auto:
            self.acker_pub.publish(self.ackermannMsg)
        else:
            self.acker_pub.publish(AckermannDriveStamped())

    #The algorithims for setting the speed and steering before publishing
    def tick(self):
        self.physics = Physics(initvel = self.acker.speed, inittime =0.0, finaltime = (1), initangle=self.acker.steering_angle, left=self.left, right=self.right, middle=self.middle_view_min)
        self.physics.turn()
        self.acker.speed = self.physics.speed
        self.acker.steering_angle = self.physics.steer


    #The start function
    def start(self, rate=20):
        r = rospy.Rate(rate)

        while not rospy.is_shutdown():

            self.tick()
            self.publish_acker()

            r.sleep()

    def shutdown(self):
        rospy.loginfo("Stopping Rudolph")
        cv2.destroyAllWindows()

        # sleep makses sure topics recive their 'stop' message fore shutting down bot
        rospy.sleep(1)

if __name__ == "__main__":
    try:
        c = Commander()
        c.start()
    except Exception as e:
        print("Exception: "+ str(e))
